#!/usr/bin/env python3

# by Ignacio Astaburuaga


import logging
import threading
import time
import queue
from Components import Block, Transaction
import sys
import random


TRANS_PER_BLOCK = 5
WORK_TIME = 60
TRANS_TO_GEN = 500
VERBOSE_LEVEL = 1



def makeTitle(strng:str, totalWdth:int = 45, filler:str = "#"):
    left = totalWdth - len(strng) - 2
    each = left//2
    sirplus = totalWdth - (each*2) - len(strng) - 2

    # print(left,each,sirplus)
    if left <= 0:
        return strng

    return "{} {} {}{}".format((filler*each), strng, (filler*each),(filler*sirplus))

def Mine(name, chain, chainLock, exitEvent, newTrans, TpB,verboseL=0):
    while not exitEvent.is_set() and not newTrans.empty():
        # print(name)
        # time.sleep(1)
        prevBlock = chain[-1]
        tTocommit = newTrans.peek(TpB)
        newBlock = Block(prevBlock.getHash(), tTocommit, genesis=False, exitEvent=exitEvent, verboseL=verboseL, name=name)
        if verboseL == 0:
            print("{}: To commit: ".format(name), newBlock.getRaw())
        solFound = newBlock.genPoW()

        if len(tTocommit) < TpB: # if we have the last few
            TpB = len(tTocommit)

        if solFound and newTrans.pop(TpB, verifyList= tTocommit):
            chainLock.acquire()
            if chain[-1] == prevBlock:
                chain.append(newBlock)
                print("{}: New Block Added: ".format(name), newBlock.getRaw())
                print("Trans Left: {}".format(newTrans.qsize()))
            chainLock.release()

    print(makeTitle("EXITED {}".format(name)))

class myQ:
    def __init__(self):
        self.mutex = threading.Lock()
        self.q = queue.Queue()
    def put(self, item):
        self.mutex.acquire()
        ret = self.q.put(item)
        self.mutex.release()
        return ret

    def pop(self, amount, verifyList=None):
        self.mutex.acquire()

        if verifyList:
            real = self.peek(amount, True)
            # print("f1")
            if amount != len(verifyList):
                self.mutex.release()
                # print("did not match")
                return False

            valid = True
            for i in range(amount):
                try:
                    test = real[i] == verifyList[i]
                except:
                    self.mutex.release()
                    return False
                    # print("real",real)
                    # print("verify",verifyList)
                    # sys.exit(1)
                valid = valid and test
                if not valid:
                    self.mutex.release()
                    return False

        # pop
        vals = list()
        for i in range(amount):
            vals.append(self.q.get())
        self.mutex.release()
        return vals

    def peek(self,amount, bypassLock=False):
        if not bypassLock:
            self.mutex.acquire()
        temp = queue.Queue()
        result = list()
        for i in range(amount):
            if not self.q.empty():
                val = self.q.get()
                temp.put(val)
                result.append(val)
            else:
                continue

        while not self.q.empty():
            val = self.q.get()
            temp.put(val)

        self.q = temp
        if not bypassLock:
            self.mutex.release()

        return result

    def empty(self):
        self.mutex.acquire()
        r = self.q.empty()
        self.mutex.release()
        return r
    def qsize(self):
        self.mutex.acquire()
        r = self.q.qsize()
        self.mutex.release()
        return r

def wait(secs:int = 60):
    print("will run for {} seconds".format(secs))
    for i in reversed(range(secs)):
        for x in range(3):
            time.sleep(0.25)
            sys.stdout.write(".")
            sys.stdout.flush()
        time.sleep(0.25)
        sys.stdout.write(str(i))
        sys.stdout.flush()
    sys.stdout.write("\n")
    sys.stdout.flush()



def genTrans(numb):
    global newTrans
    print(makeTitle("GENERATING TRANSACTIONS"))
    random.seed() # time
    for i in range(numb):
        amount = random.randint(0,100)
        f = random.randint(0,10)
        t = random.randint(0,10)
        trans = Transaction(amount,f,t)
        newTrans.put(trans)
        print(trans.getRaw())
    print(makeTitle("DONE GENERATING TRANSACTIONS"))





if __name__ == "__main__":

    exitEvent = threading.Event()
    exitEvent.clear()

    newBlockEvent = threading.Event()
    exitEvent.clear()

    newTrans = myQ()
    genTrans(TRANS_TO_GEN)

    chainLock = threading.Lock()
    Chain = list()
    #genesis
    gen = Block('',[],0,genesis=True)
    Chain.append(gen)

    alice   = threading.Thread(target=Mine, args=("Alice", Chain, chainLock, exitEvent, newTrans, TRANS_PER_BLOCK,VERBOSE_LEVEL),daemon=False )
    bob     = threading.Thread(target=Mine, args=("Bob", Chain, chainLock, exitEvent, newTrans, TRANS_PER_BLOCK,VERBOSE_LEVEL),daemon=False )
    charlie = threading.Thread(target=Mine, args=("Charlie", Chain, chainLock, exitEvent, newTrans, TRANS_PER_BLOCK,VERBOSE_LEVEL),daemon=False )

    print(makeTitle("MINING START"))
    alice.start()
    bob.start()
    charlie.start()

    alice.join()
    bob.join()
    charlie.join()

    print(makeTitle("MINING END"))

    print(makeTitle("CHAIN"))

    for block in Chain:
        print(block.getRaw())
    print(makeTitle("CHAIN END"))
