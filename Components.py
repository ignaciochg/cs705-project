#!/usr/bin/env python3

# by Ignacio Astaburuaga


import time
import threading
import random
import hashlib

class Block:

	def __init__(self,previousHash:str, transactions:list, PoW:int = None, genesis=False, exitEvent=None,verboseL:int =0,name:str = ""):
		self.previousHash = previousHash
		self.PoW = PoW
		# self.nonce = None
		self.trans = transactions
		self.time = time.time()	
		self.verified = False
		self.validatedTime = None
		self.hash = None
		self.tHash = None
		self.genesis = genesis
		self.verbose = verboseL
		self.name = name
		self.numbTrans = len(transactions)

		if not exitEvent:
			self.exitEvent = threading.Event()
			self.exitEvent.clear()
		else:
			self.exitEvent = exitEvent
		if genesis:
			self.verified = True
			self.hash= "0"* (256//4)
			self.purge()


	def genMarkleHash(self, hashes:list):
		if len(hashes) == 1:
			return hashes[0]
		count = len(hashes)
		hashes.sort()
		if count % 2 == 1:
			hashes.append(hashes[-1]) # duplicate last one to make mod2 === 0

		times = len(hashes) // 2
		sets = list()
		hashes.sort()
		for i in range(times):
			a = hashes.pop(0)
			b = hashes.pop(0)
			data = a+b
			sets.append(hashlib.sha256(data.encode()).hexdigest())

		return self.genMarkleHash(sets)


	def genHash(self):
		if not self.verified:
			ts = [t.getHash() for t in self.trans]
			self.tHash = self.genMarkleHash(ts)

			h = str(self.previousHash) + str(self.tHash) + str(self.time) + str(self.PoW) 
			self.hash = hashlib.sha256(h.encode()).hexdigest()
		return self.hash

	def setPoW(self, PoW:int):
		if not self.verified:
			self.PoW = PoW
	def validateAndLock(self):
		validated = False
		a = self.hash[-4:]
		chall = "0000"
		test = a == chall
		if self.verbose == 2:
			print("{}: {} == {}: {}".format(self.name,chall, a, test))
		if test:
			if self.verbose == 3:
				print("{}: {} == {}: {}".format(self.name,chall, a, test))
			validated = True
		if not self.verified and validated:
			self.verified = True
			self.validatedTime = time.time()
			self.purge()
		return validated

	def genPoW(self):
		random.seed() # uses time
		val = random.randint(0,1000000)
		while not self.exitEvent.is_set() and not self.verified:
			self.setPoW(val)
			self.genHash()
			worked = self.validateAndLock()
			val = val + 1
		if worked and self.verbose == 3:
			print("#############POW FOUND#############")
		return worked

	def purge(self):
		if self.verified:
			del self.trans
			return True
		else:
			return False

	def getHash(self):
		return self.hash
	def getRaw(self):
		if self.verified:
			return {
				"genesis": self.genesis,
				"prevHash": self.previousHash,
				"PoW": self.PoW,
				"CreationTime": self.time,
				"varified": self.verified,
				"validatedTime": self.validatedTime,
				"hash": self.hash,
				"transTreeHash": self.tHash,
				"miner": self.name,
				"numbTrans": self.numbTrans

			}
		else:
			return {
				"genesis": self.genesis,
				"prevHash": self.previousHash,
				"PoW": self.PoW,
				"transactions": [t.getRaw() for t in self.trans],
				"CreationTime": self.time,
				"varified": self.verified,
				"validatedTime": self.validatedTime,
				"hash": self.hash,
				"transTreeHash": self.tHash,
				"numbTrans": self.numbTrans
			}


class Transaction:
	amount = 0
	from_u = 0
	to_u = 0
	timestamp = time.time()
	def __init__(self, amount:int, from_u:int, to_u:int, thash:str = None):
		self.amount = amount
		self.from_u = from_u
		self.to_u = to_u
		if thash:
			self.hash = thash
		else:
			self.hash = self.genHash()

	def getRaw(self):
		return{
			"amount": self.amount,
			"from": self.from_u,
			"to": self.to_u,
			"time": self.timestamp,
			"hash": self.hash
		}

	def genHash(self):
		data = str(self.amount)+str(self.from_u)+str(self.to_u)+str(self.timestamp)
		self.hash = hashlib.sha256(data.encode()).hexdigest()
		return self.hash

	def getHash(self):
		return self.hash

